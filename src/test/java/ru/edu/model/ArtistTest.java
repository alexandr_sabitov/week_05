package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ArtistTest {
    Artist artist;
    Artist artist2;
    List<Album> list;


    @Before
    public void setUp() throws Exception {
        artist = new Artist("Name1");
        list = new ArrayList<>();
        list.add(new Album("TestName1", 2012));
        list.add(new Album("TestName2", 2020));
        artist.setAlbums(list);
    }

    @Test
    public void getName() {
        assertEquals("Name1",artist.getName());
    }

    @Test
    public void getAlbums() {
        artist2 = new Artist("t");
        artist2.setAlbums(list);
        assertEquals(artist.getAlbums(),artist2.getAlbums());
    }

    @Test
    public void setAlbums() {
        artist2 = new Artist("t");
        artist2.setAlbums(list);
        assertEquals(artist.getAlbums(),artist2.getAlbums());
        Album album = new Album();
    }
}