package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.RegistryExternalStorage;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;

public class RegistryXMLTest {

    RegistryExternalStorage xmlTo;
    RegistryExternalStorage xml;
    String outputFileXml = ".//output//artist_by_country.xml";
    String inputFileXml = ".//input//cd_catalog.xml";

    @Mock
    private Registry registryTest;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        registryTest = mock(Registry.class);
    }

    @Test
    public void readFrom() {
        xmlTo = new RegistryXML();
        xml = new RegistryXML();
        Registry registry = (Registry) xml.readFrom(inputFileXml);
        xmlTo.writeTo(outputFileXml,registry);
        assertNotNull(registry);
    }

    @Test
    public void testReadFromNull() {
        boolean flag = false;
        try {
            xml.readFrom(null);
        } catch (Exception e) {
            flag = true;
        }
        assertTrue(flag);

    }
    @Test
    public void testWriteNull() {
        boolean flag = false;
        try {
            xml.writeTo(null, registryTest);;
        } catch (Exception e) {
            flag = true;
        }
        assertTrue(flag);


    }


}