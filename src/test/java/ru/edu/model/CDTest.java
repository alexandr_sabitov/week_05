package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CDTest {
    CD cd;

    @Before
    public void setUp() throws Exception {
        cd = new CD("Title", "Bob D", "USA", "CompanyNames", 20.0, 2020);
    }

    @Test
    public void getTitle() {
        assertEquals("Title",cd.getTitle());
    }

    @Test
    public void getArtistName() {
        assertEquals("Bob D",cd.getArtistName());
    }

    @Test
    public void getCountry() {
        assertEquals("USA",cd.getCountry());
    }

    @Test
    public void getCompany() {
        assertEquals("CompanyNames",cd.getCompany());
    }

    @Test
    public void getPrice() {
        assertEquals(20.0,cd.getPrice(),0.001);
    }

    @Test
    public void getYear() {
        assertEquals(2020,cd.getYear());
    }

    @Test
    public void testToString() {
        assertEquals("CD{title='Title', " +
                "name='Bob D', " +
                "country='USA', " +
                "company='CompanyNames', " +
                "price=20.0, year=2020}",cd.toString());
    }

    @Test
    public void setTitle() {
        cd.setTitle("Title1");
        assertEquals("Title1",cd.getTitle());
    }

    @Test
    public void setArtistName() {
        cd.setArtistName("Artur");
        assertEquals("Artur",cd.getArtistName());
    }

    @Test
    public void setCountry() {
        cd.setCountry("Rus");
        assertEquals("Rus", cd.getCountry());
    }

    @Test
    public void setCompany() {
        cd.setCompany("Company2");
        assertEquals("Company2", cd.getCompany());
    }

    @Test
    public void setPrice() {
        cd.setPrice(2.0);
        assertEquals(2.0,cd.getPrice(),0.01);
    }

    @Test
    public void setYear() {
        cd.setYear(2000);
        assertEquals(2000,cd.getYear(),0.01);
    }
}