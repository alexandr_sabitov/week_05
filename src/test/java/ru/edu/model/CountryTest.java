package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CountryTest {

    Country country;

    @Mock
    private Artist artist1;
    @Mock
    private Artist artist2;


    @Before
    public void setUp() throws Exception {
        country = new Country("Russia");
        country.addArtists(artist1);
        artist1 = Mockito.mock(Artist.class);
        artist2 = Mockito.mock(Artist.class);
    }

    @Test
    public void getName() {
        assertEquals("Russia", country.getName());
        country.setName("USA");
        assertEquals("USA", country.getName());
    }

    @Test
    public void getArtists() {
        List<Artist> artists = new ArrayList<>();
        artists.add(artist2);
        country.setArtists(artists);
        assertEquals(artists, country.getArtists());
    }

    @Test
    public void testToString() {
        assertEquals("Country{name='Russia', artists=[null]}",country.toString());
    }
}