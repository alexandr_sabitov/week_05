package ru.edu.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CatalogTest {

    List<CD> cdList = new ArrayList<>();
    Catalog catalog;
    CD cd1;
    CD cd2;


    @Before
    public void setUp() throws Exception {
        cd1 = new CD("Title", "Bob D", "USA", "CompanyNames", 20.0, 2020);
        cd2 = new CD("Title1", "Bob D", "USA", "CompanyNames", 20.0, 2021);
        cdList.add(cd1);
        cdList.add(cd2);
        catalog = new Catalog(cdList);

    }

    @Test
    public void getCdList() {
        assertEquals(cdList, catalog.getCdList());
        String test = "Catalog{cdList=[CD{title='Title', name='Bob D', country='USA', company='CompanyNames', price=20.0, year=2020}, CD{title='Title1', name='Bob D', country='USA', company='CompanyNames', price=20.0, year=2021}]}";
        assertEquals(test, catalog.toString());
        Catalog catalog1 = new Catalog();
        assertNotNull(catalog1);
    }


    @Test
    public void test() {
        XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try (FileInputStream fis = new FileInputStream("./input/cd_catalog.xml")) {
            Catalog catalog = mapper.readValue(fis, Catalog.class);

            for (CD cd : catalog.getCdList()) {
                System.out.println(cd.toString());
            }
            assertNotNull(catalog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test2() {
        RegistryXML registryXML = new RegistryXML();
        String filePath = "./input/cd_catalog.xml";
        Registry r = (Registry) registryXML.readFrom(filePath);
        registryXML.writeTo("./output/artist_by_country.xml", r);
    }

    @Test
    public void getCounties() {
        Registry registry = new Registry();

        ObjectMapper mapper = new ObjectMapper();

        try (FileOutputStream fous = new FileOutputStream("./output/artist_by_country.json")) {
            mapper.writeValue(fous, registry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}