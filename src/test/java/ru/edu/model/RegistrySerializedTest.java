package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import ru.edu.RegistryExternalStorage;

import static org.junit.Assert.*;

public class RegistrySerializedTest {
    RegistryExternalStorage serialized;
    RegistryExternalStorage xml;
    String outputFileSerialized = ".//output//artist_by_country.serialized";
    String inputFileXml = ".//input//cd_catalog.xml";

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void readFrom() {
        serialized = new RegistrySerialized();
        xml = new RegistryXML();
        Registry registry = (Registry) xml.readFrom(inputFileXml);
        serialized.writeTo(outputFileSerialized,registry);
        assertNotNull(registry);
        assertNotNull(outputFileSerialized);
        assertNotNull(inputFileXml);

    }


}