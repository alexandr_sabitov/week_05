package ru.edu.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.*;

public class RegistryTest {

    @Mock
    private Country country1;
    @Mock
    private Country country2;

    Registry registry;

    @Before
    public void setUp() throws Exception {
        registry = new Registry();
        registry.addCountry(new Country("Usa"));
        registry.addCountry(new Country("Ru"));


    }

    @Test
    public void getCountries() {
        assertEquals(2, registry.getCountries().size());

    }

    @Test
    public void setCountries() {
        List<Country> c = new ArrayList<>();
        c.add(country1);
        c.add(country2);
        registry.setCountries(c);
        assertEquals(2, registry.getCountries().size());
        assertNotNull(registry);
        assertNotNull(registry.getCountries());
    }

    @Test
    public void testToString() {
        assertEquals("Registry{countries=" +
                "[Country{name='Usa', artists=[]}," +
                " Country{name='Ru', artists=[]}]}", registry.toString());
    }
}