package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AlbumTest {

    Album album1;
    Album album2;

    @Before
    public void setUp() throws Exception {
        album1 = new Album("TestName", 2012);
    }

    @Test
    public void testGetName() {
        assertEquals("TestName",album1.getName());
    }


    @Test
    public void testGetYear() {
        assertEquals(2012,album1.getYear());
    }

    @Test
    public void testSetName() {
        album2 = new Album("TestName1", 2012);
        album2.setName("Name");
        assertEquals("Name", album2.getName());
    }

    @Test
    public void testSetYear() {
        album2 = new Album("TestName1", 2012);
        album2.setYear(2020);
        assertEquals(2020, album2.getYear());
    }

    @Test
    public void testToString() {
        assertEquals("Album{name='TestName', year=2012}",album1.toString());
    }
}