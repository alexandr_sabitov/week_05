package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import ru.edu.RegistryExternalStorage;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;

public class RegistryJsonTest {

    RegistryExternalStorage json;
    RegistryExternalStorage xml;
    String outputFileJson = ".//output//artist_by_country.json";
    String inputFileXml = ".//input//cd_catalog.xml";

    @Mock
    private Registry registryTest;

    @Before
    public void setUp() throws Exception {

        openMocks(this);
        registryTest = mock(Registry.class);
    }

    @Test
    public void testReadFrom() {
        json = new RegistryJson();
        xml = new RegistryXML();
        Registry registry = (Registry) xml.readFrom(inputFileXml);
        json.writeTo(outputFileJson,registry);
        assertNotNull(outputFileJson);
        assertNotNull(inputFileXml);
        assertNotNull(registry);

    }





}