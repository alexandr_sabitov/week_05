package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.edu.RegistryExternalStorage;


import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.io.FileInputStream;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RegistryXML implements RegistryExternalStorage {


    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public Object readFrom(final String filePath) {
        if (filePath == null) {
            throw new IllegalArgumentException("Поле адреса файла null.");
        }
        XmlMapper mapper = new XmlMapper();
        try (FileInputStream fis = new FileInputStream(filePath)) {
            Catalog catalog = mapper.readValue(fis, Catalog.class);
            return fromCatalogByCantry(catalog);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param r        реестр
     */
    @Override
    public void writeTo(final String filePath, final Registry r) {
        XmlMapper mapper = new XmlMapper();
        try {
            mapper.writeValue(new File(filePath), r);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param c
     * @return Registry
     */
    private Registry fromCatalogByCantry(final Catalog c) {
        Map<String, List<Artist>> artists = new HashMap<>();
        Map<String, List<Album>> albums = new HashMap<>();
        List<Country> countries = new ArrayList<>();

        Set<String> countriesSet =
                c.getCdList().stream()
                        .map(CD::getCountry)
                        .distinct()
                        .collect(Collectors.toCollection(LinkedHashSet::new));

        for (CD cd : c.getCdList()) {

            if (!artists.containsKey(cd.getCountry())) {
                artists.put(cd.getCountry(), new ArrayList<>());
            }
            artists.get(cd.getCountry()).add(new Artist(cd.getArtistName()));

            if (!albums.containsKey(cd.getArtistName())) {
                albums.put(cd.getArtistName(), new ArrayList<>());
            }
            albums.get(cd.getArtistName())
                    .add(new Album(cd.getTitle(), cd.getYear()));
        }

        for (List<Artist> countryArtists : artists.values()) {
            for (Artist artist : countryArtists) {
                List<Album> albumsTemp = albums.get(artist.getName());
                artist.setAlbums(albumsTemp);
            }
        }
        for (String s : countriesSet) {
            Country country = new Country(s);
            country.setArtists(artists.get(s));
            countries.add(country);
        }
        Registry r = new Registry();
        r.setCountries(countries);
        return r;

    }

}
