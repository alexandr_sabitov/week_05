package ru.edu.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.edu.RegistryExternalStorage;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;


public class RegistryJson implements RegistryExternalStorage {
    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public Object readFrom(final String filePath) {
        if (filePath == null) {
            throw new IllegalArgumentException("Поле адреса файла null.");
        }

        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = streamToString(new FileInputStream(filePath));
            Registry registry = mapper.readValue(json, Registry.class);
            return registry;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(final String filePath, final Registry registry) {
        if (filePath == null) {
            throw new IllegalArgumentException("Поле адреса файла null.");
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(filePath), registry);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Преобразовать поток в строку.
     *
     * @param inputStream путь
     * @return sb
     */

    private String streamToString(final InputStream inputStream)
            throws Exception {

        StringBuilder stringBuilder = new StringBuilder();
        String line;
        BufferedReader bufferedReader =
                new BufferedReader(new InputStreamReader(inputStream));
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        bufferedReader.close();
        return stringBuilder.toString();

    }
}
