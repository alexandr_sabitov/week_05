package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "CATALOG")
public class Catalog {
    /**
     * List CD.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "CD")
    private List<CD> cdList = new ArrayList<>();

    /**
     * Контруктор по умолчанию.
     */
    public Catalog() {
    }

    /**
     * Контруктор с параметром списка CD.
     *
     * @param cdListIn
     */
    public Catalog(final List<CD> cdListIn) {
        this.cdList = cdListIn;
    }

    /**
     * Список CD.
     *
     * @return cdList
     */
    public List<CD> getCdList() {
        return cdList;
    }

    /**
     * строковое представление.
     *
     * @return
     */
    @Override
    public String toString() {
        return "Catalog{" + "cdList=" + cdList + '}';
    }
}
