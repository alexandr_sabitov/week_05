package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;

public class Album implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Название альбома.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    /**
     * Год выпуска альбома.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "year", isAttribute = true)
    private long year;

    /**
     * Конструктор альбома.
     *
     * @param nameIn
     * @param yearIn
     */
    public Album(final String nameIn, final long yearIn) {
        this.name = nameIn;
        this.year = yearIn;
    }

    /**
     * Конструктор альбома default.
     */
    public Album() {
    }

    /**
     * геттер имени альбома.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * геттер года выпуска альбома.
     *
     * @return year
     */
    public long getYear() {
        return year;
    }

    /**
     * передать имя альбома.
     *
     * @param nameIn
     */

    public void setName(final String nameIn) {
        this.name = nameIn;
    }

    /**
     * передать год выпуска.
     *
     * @param yearIn
     */
    public void setYear(final long yearIn) {
        this.year = yearIn;
    }

    /**
     * toString.
     *
     * @return
     */
    @Override
    public String toString() {
        return "Album{"
                + "name='" + name + '\''
                + ", year=" + year + '}';
    }
}
