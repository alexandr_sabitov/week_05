package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Registry implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Список стран.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("Country")
    private List<Country> countries = new ArrayList<>();

    /**
     * Геттер, возвращает список стран.
     *
     * @return countries
     */
    public List<Country> getCountries() {
        return countries;
    }

    /**
     * Передача списка стран.
     *
     * @param countriesIn
     */
    public void setCountries(final List<Country> countriesIn) {
        this.countries = countriesIn;
    }

    /**
     * Добавление страны в список.
     *
     * @param country
     */
    public void addCountry(final Country country) {
        countries.add(country);
    }

    /**
     * Конструктор по умолчанию.
     */
    public Registry() {
    }

    /**
     * @return toString
     */
    @Override
    public String toString() {
        return "Registry{" + "countries=" + countries + '}';
    }
}
