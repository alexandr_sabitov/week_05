package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CD {
    /**
     * Название альбома.
     */
    @JacksonXmlProperty(localName = "TITLE")
    private String title;

    /**
     * Имя артиста.
     */
    @JacksonXmlProperty(localName = "ARTIST")
    private String artistName;

    /**
     * Название страны.
     */
    @JacksonXmlProperty(localName = "COUNTRY")
    private String country;

    /**
     * Наименование компании.
     */
    @JacksonXmlProperty(localName = "COMPANY")
    private String company;

    /**
     * Цена.
     */
    @JacksonXmlProperty(localName = "PRICE")
    private double price;

    /**
     * Год выпуска.
     */
    @JacksonXmlProperty(localName = "YEAR")
    private int year;

    /**
     * Конструктор по умолчанию.
     */
    public CD() {
    }

    /**
     * Конструктор с параметрами.
     *
     * @param title1
     * @param name1
     * @param country1
     * @param company1
     * @param price1
     * @param year1
     */
    public CD(final String title1, final String name1,
              final String country1, final String company1,
              final double price1, final int year1) {
        this.title = title1;
        this.artistName = name1;
        this.country = country1;
        this.company = company1;
        this.price = price1;
        this.year = year1;
    }

    /**
     * Возвращает название альбома.
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Возвращает имя исполнителя.
     *
     * @return name
     */
    public String getArtistName() {
        return artistName;
    }

    /**
     * Возвращает страну.
     *
     * @return country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Возвращает компанию.
     *
     * @return company
     */
    public String getCompany() {
        return company;
    }

    /**
     * Возвращает цену.
     *
     * @return price
     */
    public double getPrice() {
        return price;
    }

    /**
     * Возвращает год выпуска.
     *
     * @return year
     */
    public int getYear() {
        return year;
    }

    /**
     * @return toString
     */
    @Override
    public String toString() {
        return "CD{" + "title='" + title + '\'' + ", name='" + artistName
                + '\'' + ", country='" + country + '\''
                + ", company='" + company + '\'' + ", price=" + price
                + ", year=" + year + '}';
    }

    /**
     *
     * @param titleIn
     */
    public void setTitle(final String titleIn) {
        this.title = titleIn;
    }

    /**
     *
     * @param artistNameIn
     */
    public void setArtistName(final String artistNameIn) {
        this.artistName = artistNameIn;
    }

    /**
     *
     * @param countryIn
     */
    public void setCountry(final String countryIn) {
        this.country = countryIn;
    }

    /**
     *
     * @param companyIn
     */
    public void setCompany(final String companyIn) {
        this.company = companyIn;
    }

    /**
     *
     * @param priceIn
     */
    public void setPrice(final double priceIn) {
        this.price = priceIn;
    }

    /**
     *
     * @param yearIn
     */
    public void setYear(final int yearIn) {
        this.year = yearIn;
    }
}
