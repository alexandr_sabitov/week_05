package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Country implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Название страны.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "name")
    private String name;

    /**
     * Список исполнителей.
     */
    @JsonProperty()
    @JacksonXmlElementWrapper(localName = "artist", useWrapping = false)
    @JacksonXmlProperty(localName = "artist")
    private List<Artist> artists = new ArrayList<>();

    /**
     *
     * @param nameIn
     */
    public Country(final String nameIn) {
        this.name = nameIn;
    }

    /**
     * Конструктор  по умолчанию.
     */
    public Country() {
    }

    /**
     * Вернуть название страны.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Передать название страны.
     *
     * @param nameIn
     */
    public void setName(final String nameIn) {
        this.name = nameIn;
    }

    /**
     * Вернуть список исполнителей.
     *
     * @return artists
     */
    public List<Artist> getArtists() {
        return artists;
    }

    /**
     * Передача списка исполнителей.
     *
     * @param artistsIn
     */
    public void setArtists(final List<Artist> artistsIn) {
        this.artists = artistsIn;
    }

    /**
     * Добавление исполнителя.
     *
     * @param artistIn
     */
    public void addArtists(final Artist artistIn) {
        this.artists.add(artistIn);
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "Country{" + "name='" + name
                + '\'' + ", artists=" + artists + '}';
    }
}
