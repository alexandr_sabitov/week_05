package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Artist implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Имя исполнителя.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    /**
     * Список альбомов исполнителя.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "album", isAttribute = true)
    private List<Album> albums = new ArrayList<>();

    /**
     * Контруктор с параметрами.
     * @param nameIn
     */
    public Artist(final String nameIn) {
        this.name = nameIn;

    }

    /**
     * Контруктор по умолчанию.
     */
    public Artist() {
    }

    /**
     * Геттер имени.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Геттер по списку альбомов.
     * @return albums
     */
    public List<Album> getAlbums() {
        return albums;
    }

    /**
     * Передать список альбомов.
     * @param albums1
     */
    public void setAlbums(final List<Album> albums1) {
        this.albums = albums1;
    }
}

