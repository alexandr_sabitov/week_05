package ru.edu.model;

import ru.edu.RegistryExternalStorage;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;


public class RegistrySerialized implements RegistryExternalStorage {
    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     */
    @Override
    public Object readFrom(final String filePath) {
        if (filePath == null) {
            throw new IllegalArgumentException("Поле адреса файла null.");
        }

        Registry registry;

        try (FileInputStream inputStream
                     = new FileInputStream(filePath);
             ObjectInputStream objectInputStream
                     = new ObjectInputStream(inputStream)) {
            registry = (Registry) objectInputStream.readObject();
            return registry;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(final String filePath, final Registry registry) {
        if (filePath == null) {
            throw new IllegalArgumentException("Поле адреса файла null.");
        }
        try (FileOutputStream outputStream
                     = new FileOutputStream(filePath);
             ObjectOutputStream objectOutputStream
                     = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(registry);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
